﻿using Academy.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Contracts.Response
{
    public class DepartamentoResponse : BaseResponse
    {
        public Endereco Endereco { get; set; }
        public string Nome { get; set; }
    }
}
