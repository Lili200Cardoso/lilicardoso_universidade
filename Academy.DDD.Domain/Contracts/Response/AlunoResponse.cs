﻿using Academy.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Contracts.Response
{
    public class AlunoResponse : BaseResponse
    {
        public Usuario Usuario { get; set; }
        public  Curso Curso { get; set; }
        public int Matricula { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
    }
}
