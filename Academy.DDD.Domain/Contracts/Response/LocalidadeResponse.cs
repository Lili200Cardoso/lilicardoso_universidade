﻿namespace Academy.DDD.Domain.Contracts.Response
{
    public class LocalidadeResponse
    {
        public string Cep { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string Rua { get; set; }
        public string Servico { get; set; }
        public string Local { get; set; }

    }
}
