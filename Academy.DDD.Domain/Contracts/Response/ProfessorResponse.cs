﻿using Academy.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Contracts.Response
{
    public class ProfessorResponse : BaseResponse
    {
        public Usuario Usuario { get; set; }
        public  Departamento Departamento { get; set; }
        public float Salario { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
    }
}
