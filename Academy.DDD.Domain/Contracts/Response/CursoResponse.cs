﻿using Academy.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Contracts.Response
{
    public class CursoResponse : BaseResponse
    {
        public Departamento Departamento { get; set; }
        public string Turno { get; set; }
        public int Duracao { get; set; }
        public string TipoCurso { get; set; }
    }
}
