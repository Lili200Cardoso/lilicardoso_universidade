﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Entities
{
    public class Professor : BaseEntity
    {
        public int? UsuarioId { get; set; }
        public virtual Usuario Usuario { get; set; }
        public int? DepartamentoId { get; set; }
        public virtual Departamento Departamento {get; set;}
        public float Salario { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
    }
}
