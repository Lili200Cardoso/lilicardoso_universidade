﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;

namespace Academy.DDD.Domain.Services
{
    public class ProfessorService : BaseService<Professor>, IProfessorService
    {
        private readonly IProfessorRepository _professorRepository;
        public ProfessorService(IProfessorRepository professorRepository, IHttpContextAccessor httpContextAccessor) : base(professorRepository, httpContextAccessor)
        {
            _professorRepository = professorRepository;
    
        }

        public async Task AtualizarSalarioAsync(int id, float salario)
        {
            var entity = await ObterPorIdAsync(id);
            entity.Salario = salario;
            entity.DataAlteracao = DateTime.Now;
            entity.UsuarioAlteracao = UserId;
            await _professorRepository.EditAsync(entity);
        }
    }
}
