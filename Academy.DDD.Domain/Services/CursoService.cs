﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Services
{
    public class CursoService : BaseService<Curso>, ICursoService
    {
        private readonly ICursoRepository _cursoRepository;
        public CursoService(ICursoRepository cursoRepository, IHttpContextAccessor httpContextAccessor) : base(cursoRepository, httpContextAccessor)
        {
            _cursoRepository = cursoRepository;
        }

        public async Task AtualizarTipoCursoAsync(int id, string tipoCurso)
        {
            var entity = await ObterPorIdAsync(id);
            entity.TipoCurso = tipoCurso;
            entity.DataAlteracao = DateTime.Now;
            entity.UsuarioAlteracao = UserId;
            await _cursoRepository.EditAsync(entity);
        }
    }
}
