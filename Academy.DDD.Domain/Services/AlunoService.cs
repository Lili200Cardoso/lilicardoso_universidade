﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;

namespace Academy.DDD.Domain.Services
{
    public class AlunoService : BaseService<Aluno>, IAlunoService
    {
        private readonly IAlunoRepository _alunoRepository;
        public AlunoService(IAlunoRepository alunoRepository, IHttpContextAccessor httpContextAccessor) : base(alunoRepository, httpContextAccessor)
        {
            _alunoRepository = alunoRepository;
        }

        public async Task AtualizarMatriculaAsync(int id, int matricula)
        {
            var entity = await ObterPorIdAsync(id);
            entity.Matricula = matricula;
            entity.DataAlteracao = DateTime.Now;
            entity.UsuarioAlteracao = UserId;
            await _alunoRepository.EditAsync(entity);
        }
    }
}
