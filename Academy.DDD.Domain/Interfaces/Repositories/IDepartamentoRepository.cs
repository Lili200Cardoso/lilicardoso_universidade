﻿using Academy.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Interfaces.Repositories
{
    public interface IDepartamentoRepository : IBaseRepository<Departamento>
    {
    }
}
