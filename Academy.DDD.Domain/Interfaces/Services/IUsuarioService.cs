﻿using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;

namespace Academy.DDD.Domain.Interfaces.Services
{
    public interface IUsuarioService : IBaseService<Usuario>
    {
        Task CriarUsuarioAsync(Usuario usuario);
        Task AtualizarUsuarioAsync(Usuario usuario);
        Task<AutenticacaoResponse> AutenticarAsync(string email, string senha);
        Task AtualizarNomeAsync(int id, string nome);
        Task<List<Usuario>> ObterTodosUsuarioAsync();
        Task<Usuario> ObterPorIdUsuarioAsync(int id);
      
    }
}
