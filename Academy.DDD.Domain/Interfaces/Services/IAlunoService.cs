﻿using Academy.DDD.Domain.Entities;

namespace Academy.DDD.Domain.Interfaces.Services
{
    public interface IAlunoService : IBaseService<Aluno>
    {
        Task AtualizarMatriculaAsync(int id, int matricula);
    }
}
