﻿using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Academy.DDD.API.Controllers
{
    public class UsuariosController : BaseController<Usuario, UsuarioRequest, UsuarioResponse>
    {
        private readonly IMapper _mapper;
        private readonly IUsuarioService _usuarioService;

        public UsuariosController(IMapper mapper, IUsuarioService service) : base(mapper, service)
        {
            _mapper = mapper;
            _usuarioService = service;
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(201)]
        public override async Task<ActionResult> PostAsync([FromBody] UsuarioRequest request)
        {
            var entity = _mapper.Map<Usuario>(request);
            await _usuarioService.CriarUsuarioAsync(entity);
            return Created(nameof(PostAsync), new { id = entity.Id });
        }

        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] UsuarioNomeRequest request)
        {
            await _usuarioService.AtualizarNomeAsync(id, request.Nome);
            return Ok();
        }

        [HttpGet("nome")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<UsuarioResponse>>> GetAsync([FromQuery] string nome)
        {
            var entities = await _usuarioService.ObterTodosAsync(x => x.Nome.Contains(nome));
            var response = _mapper.Map<List<UsuarioResponse>>(entities);
            return Ok(response);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        public override async Task<ActionResult> PutAsync([FromRoute] int id, [FromBody] UsuarioRequest request)
        {
            var entity = _mapper.Map<Usuario>(request);
            entity.Id = id;
            await _usuarioService.AtualizarUsuarioAsync(entity);
            return NoContent();
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public override async Task<ActionResult<UsuarioResponse>> GetByIdAsync([FromRoute] int id)
        {
            var entity = await _usuarioService.ObterPorIdUsuarioAsync(id);
            return Ok(_mapper.Map<UsuarioResponse>(entity));
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public override async Task<ActionResult<List<UsuarioResponse>>> GetAsync()
        {
            var entity = await _usuarioService.ObterTodosUsuarioAsync();
            return Ok(_mapper.Map<List<UsuarioResponse>>(entity));
        }


    }
}
