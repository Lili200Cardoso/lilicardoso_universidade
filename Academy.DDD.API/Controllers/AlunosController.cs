﻿using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static Academy.DDD.Domain.Utils.ConstanteUtil;

namespace Academy.DDD.API.Controllers
{
    [Authorize(Roles = PerfilAlunoNome)]
    public class AlunosController : BaseController<Aluno, AlunoRequest, AlunoResponse>
    {
        private readonly IMapper _mapper;
        private readonly IAlunoService _alunoService;
        public AlunosController(IMapper mapper, IAlunoService alunoService) : base(mapper, alunoService)
        {
            _mapper = mapper;
            _alunoService = alunoService;
        }

        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] AlunoMatriculaRequest request)
        {
            await _alunoService.AtualizarMatriculaAsync(id, request.Matricula);
            return Ok();
        }

        [HttpGet("matricula")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<AlunoResponse>>> GetAsync([FromQuery] int matricula)
        {
            var entities = await _alunoService.ObterTodosAsync(x => x.Matricula.Equals(matricula));
            var response = _mapper.Map<List<AlunoResponse>>(entities);
            return Ok(response);
        }
    }
}
