﻿using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Enums;
using Academy.DDD.Domain.Exceptions;
using Academy.DDD.Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Academy.DDD.API.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override Task OnExceptionAsync(ExceptionContext context)
        {
            var response = new InformacaoResponse();

            if (context.Exception is InformacaoException)
            {
                var informacaoException = (InformacaoException)context.Exception;

                response.Codigo = informacaoException.Codigo;
                response.Mensagens = informacaoException.Mensagens;
                response.Detalhe = context.Exception?.InnerException?.Message;
            }
            else
            {
                response.Codigo = StatusException.Erro;
                response.Mensagens = new List<string> { "Erro Não Tratado" };
                response.Detalhe = context.Exception?.InnerException?.Message;
            }

            context.Result = new ObjectResult(response)
            {
                StatusCode = response.Codigo.GetStatusCode()
            };

            OnException(context);
            return Task.CompletedTask;
        }
    }
}
