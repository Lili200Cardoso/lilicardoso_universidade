﻿using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using AutoMapper;

namespace Academy.DDD.API.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Entity to Request

            CreateMap<UsuarioRequest, Usuario>();
            CreateMap<AlunoRequest, Aluno>();
            CreateMap<CursoRequest, Curso>();
            CreateMap<DepartamentoRequest, Departamento>();
            CreateMap<EnderecoRequest, Endereco>();
            CreateMap<PerfilRequest, Perfil>();
            CreateMap<ProfessorRequest, Professor>();

            #endregion

            #region Response to Entity 

            CreateMap<Usuario, UsuarioResponse>();
            CreateMap<Aluno, AlunoResponse>();
            CreateMap<Curso, CursoResponse>();
            CreateMap<Departamento, DepartamentoResponse>();
            CreateMap<Endereco, EnderecoResponse>();
            CreateMap<Perfil, PerfilResponse>();
            CreateMap<Professor, ProfessorResponse>();
            CreateMap<Localidade, LocalidadeResponse>()
                .ForMember(dst => dst.Cep, map => map.MapFrom(src => src.Cep.ToString()))
                .ForMember(dst => dst.Estado, map => map.MapFrom(src => src.State.ToString()))
                .ForMember(dst => dst.Cidade, map => map.MapFrom(src => src.City.ToString()))
                .ForMember(dst => dst.Bairro, map => map.MapFrom(src => src.Neighborhood.ToString()))
                .ForMember(dst => dst.Rua, map => map.MapFrom(src => src.Street.ToString()))
                .ForMember(dst => dst.Servico, map => map.MapFrom(src => src.Service.ToString()))
                .ForMember(dst => dst.Local, map => map.MapFrom(src => src.Location.ToString()));
;
            #endregion
        }
    }
}
