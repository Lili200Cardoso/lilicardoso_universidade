﻿using Academy.DDD.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Academy.DDD.Infrastructure.Contexts
{
    public class AcademyContext : DbContext
    {
        public AcademyContext(DbContextOptions<AcademyContext> options) : base(options)
        {
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Perfil> Perfis { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<Departamento> Departamentos { get; set; } 
        public DbSet<Curso> Cursos { get; set; }
        public DbSet<Aluno> Alunos { get; set; }
        public DbSet<Professor> Professores { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Professor>().HasOne(p => p.Departamento).WithOne().OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Professor>().HasOne(p => p.Usuario).WithOne().OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Aluno>().HasOne(a => a.Usuario).WithOne().OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Usuario>().HasOne(p => p.Endereco).WithOne().OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Departamento>().HasOne(d => d.Endereco).WithOne().OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Usuario>().HasOne(a => a.Endereco).WithOne().OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Usuario>().HasOne(a => a.Perfil).WithOne().OnDelete(DeleteBehavior.Restrict);
           
        }


    }
}
