﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Infrastructure.Contexts;

namespace Academy.DDD.Infrastructure.Repositories
{
    public class AlunoRepository : BaseRepository<Aluno>, IAlunoRepository
    {
        public AlunoRepository(AcademyContext academyContext) : base(academyContext)
        {
        }
    }
}
