﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Infrastructure.Contexts;

namespace Academy.DDD.Infrastructure.Repositories
{
    public class CursoRepository : BaseRepository<Curso>, ICursoRepository
    {
        public CursoRepository(AcademyContext academyContext) : base(academyContext)
        {
        }
    }
}
