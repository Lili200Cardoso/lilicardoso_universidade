﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;

namespace Academy.DDD.Infrastructure.Repositories
{
    public class LocalidadeRepository : BaseApiRepository, ILocalidadeRepository
    {
        public LocalidadeRepository(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<Localidade> GetLocalidadeAsync(string cep)
        {
            return await GetAsync<Localidade>($"/api/cep/v2/{cep}");
        }
    }
}
